
penguin_missile.png
size: 512,128
format: RGBA8888
filter: Linear,Linear
repeat: none
penguin/exhaust_fire
  rotate: false
  xy: 2, 2
  size: 94, 40
  orig: 94, 40
  offset: 0, 0
  index: -1
penguin/explosion
  rotate: false
  xy: 268, 33
  size: 80, 76
  orig: 80, 76
  offset: 0, 0
  index: -1
penguin/missile_icicle
  rotate: false
  xy: 2, 44
  size: 131, 65
  orig: 131, 65
  offset: 0, 0
  index: -1
penguin/missile_projectile
  rotate: false
  xy: 135, 44
  size: 131, 65
  orig: 131, 65
  offset: 0, 0
  index: -1
