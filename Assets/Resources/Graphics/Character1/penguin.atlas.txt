
penguin.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
core_bottom
  rotate: false
  xy: 172, 160
  size: 211, 124
  orig: 211, 124
  offset: 0, 0
  index: -1
core_top_frame
  rotate: false
  xy: 2, 330
  size: 195, 180
  orig: 195, 180
  offset: 0, 0
  index: -1
engine
  rotate: true
  xy: 389, 359
  size: 151, 99
  orig: 151, 99
  offset: 0, 0
  index: -1
engine_coat
  rotate: true
  xy: 385, 162
  size: 122, 125
  orig: 122, 125
  offset: 0, 0
  index: -1
engine_exhaust
  rotate: true
  xy: 389, 41
  size: 43, 73
  orig: 43, 73
  offset: 0, 0
  index: -1
exhaust_fire
  rotate: true
  xy: 466, 66
  size: 94, 40
  orig: 94, 40
  offset: 0, 0
  index: -1
feign_flag
  rotate: false
  xy: 2, 2
  size: 97, 136
  orig: 97, 136
  offset: 0, 0
  index: -1
fist
  rotate: false
  xy: 389, 86
  size: 75, 74
  orig: 75, 74
  offset: 0, 0
  index: -1
fist_chain
  rotate: true
  xy: 352, 29
  size: 129, 35
  orig: 129, 35
  offset: 0, 0
  index: -1
icewall
  rotate: false
  xy: 2, 140
  size: 168, 188
  orig: 168, 188
  offset: 0, 0
  index: -1
pilot_arm_control
  rotate: true
  xy: 101, 28
  size: 110, 101
  orig: 110, 101
  offset: 0, 0
  index: -1
pilot_penguin
  rotate: true
  xy: 204, 19
  size: 139, 146
  orig: 139, 146
  offset: 0, 0
  index: -1
pilot_seat
  rotate: false
  xy: 199, 359
  size: 188, 151
  orig: 188, 151
  offset: 0, 0
  index: -1
sled
  rotate: false
  xy: 199, 286
  size: 250, 71
  orig: 250, 71
  offset: 0, 0
  index: -1
