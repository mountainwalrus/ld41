﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character2Skills : Skills {

	public override void Attack(int value){
		if (value == 0) {
			GameObject other;
			int change;
			if (gameObject == GameManager.player) {
				other = GameManager.enemy;
			} else {
				other = GameManager.player;
			}
			if (gameObject.GetComponent<Position> ().position > other.GetComponent<Position> ().position) {
				change = gameObject.GetComponent<Position> ().position - other.GetComponent<Position> ().position;
			} else {
				change = other.GetComponent<Position> ().position - gameObject.GetComponent<Position> ().position;
			}
			attackMoveBefore = change;
		}
	}
		
	public override void Dodge(int value){

	}

	public override void Feign(int value){
		if (value == 1) {
			IncreasePower (50);
		}
	}

	public override void Block(int value){
		if (value == 1) {
			IncreasePower (25);
		}
	} 

	public override void Special(int value){
		if (value == 0) {
			StartCoroutine (gameObject.GetComponent<Position> ().MovePosition (-4));
		}
	}
}
