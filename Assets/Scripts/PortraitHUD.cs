﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortraitHUD : MonoBehaviour {

	public Sprite frameTiny;
	public Sprite hightlightFrameTiny;

	public List<Image> frames;
	public List<Image> portraits;

	void Awake(){
		foreach (Transform child in transform) {
				portraits.Add (child.gameObject.GetComponent<Image> ());
		}
		foreach (Image image in portraits) {
			foreach (Transform child in image.gameObject.transform) {
				frames.Add (child.gameObject.GetComponent<Image> ());
			}
		}


	}
		
	void Start(){
		ChoiceMenu choiseMenu = GetComponentInParent<ChoiceMenu> ();
		for(int x = 0; x < choiseMenu.characters.Count; x++){
			Image imageComponent = portraits [x];
			GameObject character = choiseMenu.characters [x];
			imageComponent.sprite = character.GetComponent<Character> ().miniPortrait;
		}
	}


	public void MouseEnter(int value){
		if (!ChoiceMenu.isPlayerSelected || !ChoiceMenu.isEnemySelected) {
			GetComponentInParent<ChoiceMenu> ().DisplayCharacter (value);
		}
	}

	public void MouseExit(){
		if (!ChoiceMenu.isPlayerSelected || !ChoiceMenu.isEnemySelected) {
			GetComponentInParent<ChoiceMenu> ().HideCharacter ();
		}
	}

	public void SelectCharacter(int value){
		if (!ChoiceMenu.isPlayerSelected || !ChoiceMenu.isEnemySelected) {
			GetComponentInParent<ChoiceMenu> ().SelectCharacter (value);
		}
	}
}
