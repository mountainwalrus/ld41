﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CombatManager : MonoBehaviour {

	int[,] counters;
	bool[,] playAnimations;

	public static int advantage = 50;



	public IEnumerator ExecuteSkillSequence(){
		GameObject player = GameManager.player;
		GameObject enemy = GameManager.enemy;
		bool isBattleOver = false;

		for (int x = 0; x < 3; x++) {
			if(isBattleOver){
				break;
			}
			Skill playerSkill = player.GetComponent<PlayerInput> ().queuedActions [x];

			Skill enemySkill = enemy.GetComponent<AIInput> ().ReturnEnemySkillChoise (playerSkill, x);

			GameObject winner = ReturnSuperiorSkill(player, playerSkill, enemy, enemySkill);


			yield return StartCoroutine(HUDManager.enemyActionHUD.FlipSkillEnemy(x, enemySkill));
			if (winner == player) {
				HUDManager.playerSuccess.TriggerSuccess (x);
				HUDManager.enemySuccess.TriggerFailure (x);
			} else {
				HUDManager.playerSuccess.TriggerFailure (x);
				HUDManager.enemySuccess.TriggerSuccess (x);
			}


			// This is the first Movement Phase.
			/*
			if (winner == player) {
				ExecuteSkill (player, playerSkill, 0);
				yield return StartCoroutine (player.GetComponent<Position> ().MovePosition (player.GetComponent<Skills>().ReturnDistanceBefore(player, playerSkill)));
			} else {
				ExecuteSkill (enemy, enemySkill, 0);
				yield return StartCoroutine (enemy.GetComponent<Position> ().MovePosition (enemy.GetComponent<Skills>().ReturnDistanceBefore(enemy, enemySkill)));
			}
			*/
			if (winner == player) {
				ExecuteSkill (player, playerSkill, 0);
			}
			StartCoroutine (player.GetComponent<Position> ().MovePosition (player.GetComponent<Skills>().ReturnDistanceBefore(player, playerSkill)));
			if (winner == enemy) {
				ExecuteSkill (enemy, enemySkill, 0);
			}
			yield return StartCoroutine (enemy.GetComponent<Position> ().MovePosition (enemy.GetComponent<Skills>().ReturnDistanceBefore(enemy, enemySkill)));

			yield return new WaitForSeconds (0.15f);

			// Skirmish check!
			if (player.GetComponent<Position> ().position == enemy.GetComponent<Position> ().position) {
				if (player.GetComponent<Character> ().agility > enemy.GetComponent<Character> ().agility) {
					yield return StartCoroutine (enemy.GetComponent<Position> ().MovePosition (-1));
				} else {
					yield return StartCoroutine (player.GetComponent<Position> ().MovePosition (-1));
				}
			}
			if (player.GetComponent<Position> ().position == enemy.GetComponent<Position> ().position) {
				if (player.GetComponent<Character> ().agility > enemy.GetComponent<Character> ().agility) {
					yield return StartCoroutine (enemy.GetComponent<Position> ().MovePosition (1));
				} else {
					yield return StartCoroutine (enemy.GetComponent<Position> ().MovePosition (1));
				}
			}



			yield return new WaitForSeconds (0.35f);

			// this is the combat phase (animation)
			bool doesPlayerAnimationTrigger = playAnimations [(int)playerSkill, (int)enemySkill];
			if (player == winner) {
				doesPlayerAnimationTrigger = true;
				StartCoroutine (GetComponent<GameManager>().playerText.GetComponent<CharacterText> ().DisplayText (playerSkill.ToString().ToUpper(), true));
				StartCoroutine (GetComponent<GameManager>().enemyText.GetComponent<CharacterText> ().DisplayText (enemySkill.ToString().ToUpper(), false));
			}
			bool doesEnemyAnimationTrigger = playAnimations [(int)enemySkill, (int)playerSkill];
			if (enemy == winner) {
				doesEnemyAnimationTrigger = true;
				StartCoroutine (GetComponent<GameManager>().enemyText.GetComponent<CharacterText> ().DisplayText (enemySkill.ToString().ToUpper(), true));
				StartCoroutine (GetComponent<GameManager>().playerText.GetComponent<CharacterText> ().DisplayText (playerSkill.ToString().ToUpper(), false));
			}
			float playerDuration;
			float enemyDuration;
			if (doesPlayerAnimationTrigger) {
				if (winner == player) {
					ExecuteSkill (player, playerSkill, 1);
				}
				StartCoroutine (player.GetComponent<Animations> ().PlayAnimation (playerSkill.ToString().ToLower()));
				playerDuration = player.GetComponent<Animations> ().ReturnAnimationDuration (playerSkill.ToString().ToLower());
			} else {
				StartCoroutine (player.GetComponent<Animations> ().PlayAnimation ("hit"));
				playerDuration = player.GetComponent<Animations> ().ReturnAnimationDuration ("hit");
			}
			if (doesEnemyAnimationTrigger) {
				if (winner == enemy) {
					ExecuteSkill (enemy, enemySkill, 1);
				}
				StartCoroutine (enemy.GetComponent<Animations> ().PlayAnimation (enemySkill.ToString().ToLower()));
				enemyDuration = enemy.GetComponent<Animations> ().ReturnAnimationDuration (enemySkill.ToString().ToLower());
			} else {
				StartCoroutine (enemy.GetComponent<Animations> ().PlayAnimation ("hit"));
				enemyDuration = enemy.GetComponent<Animations> ().ReturnAnimationDuration ("hit");
			}

			float animationDuration;
			if (playerDuration > enemyDuration) {
				animationDuration = playerDuration;
			} else {
				animationDuration = enemyDuration;
			}

			// This is the Combat Phase (advantage)
			if (winner == player) {
				ModifyAdvantage (true, player, playerSkill, enemy);
			} else {
				ModifyAdvantage (false, enemy, enemySkill, player);
			}
			HUDManager.advantageHUD.UpdateAdvantageHUD();



			yield return new WaitForSeconds(animationDuration);
			yield return new WaitForSeconds (0.1f);

			// This checks if combat is over.
			if (advantage == 100) {
				isBattleOver = true;
				yield return StartCoroutine (HUDManager.matchTextHUD.Fatality ());
			} else if (advantage == 0) {
				isBattleOver = true;
			}
			if (isBattleOver) {
				yield return StartCoroutine (EXECUTE(winner));
				GetComponent<GameManager> ().SetMatchAsInactive ();

				yield return StartCoroutine (HUDManager.matchTextHUD.Victory (winner));
				yield return new WaitForSeconds (5);
				SceneManager.LoadScene ("MainMenu");
			}

			// This is the secondd move phase.
			/*
			if (winner == player) {
				ExecuteSkill (player, playerSkill, 2);
				yield return StartCoroutine (player.GetComponent<Position> ().MovePosition (player.GetComponent<Skills>().ReturnDistanceAfter(player, playerSkill)));
			} else {
				ExecuteSkill (enemy, enemySkill, 2);
				yield return StartCoroutine (enemy.GetComponent<Position> ().MovePosition (enemy.GetComponent<Skills>().ReturnDistanceAfter(enemy, enemySkill)));
			}
			*/
			if (winner == player) {
				ExecuteSkill (player, playerSkill, 2);
			}
			StartCoroutine (player.GetComponent<Position> ().MovePosition (player.GetComponent<Skills>().ReturnDistanceAfter(player, playerSkill)));
			if (winner == enemy) {
				ExecuteSkill (enemy, enemySkill, 2);
			}
			yield return StartCoroutine (enemy.GetComponent<Position> ().MovePosition (enemy.GetComponent<Skills>().ReturnDistanceAfter(enemy, enemySkill)));


			yield return new WaitForSeconds (0.25f);

			// Skirmish check!
			if (player.GetComponent<Position> ().position == enemy.GetComponent<Position> ().position) {
				if (player.GetComponent<Character> ().agility > enemy.GetComponent<Character> ().agility) {
					yield return StartCoroutine (enemy.GetComponent<Position> ().MovePosition (-1));
				} else {
					yield return StartCoroutine (player.GetComponent<Position> ().MovePosition (-1));
				}
			}
			if (player.GetComponent<Position> ().position == enemy.GetComponent<Position> ().position) {
				if (player.GetComponent<Character> ().agility > enemy.GetComponent<Character> ().agility) {
					yield return StartCoroutine (enemy.GetComponent<Position> ().MovePosition (1));
				} else {
					yield return StartCoroutine (enemy.GetComponent<Position> ().MovePosition (1));
				}
			}

			yield return new WaitForSeconds (0.1f);

			yield return new WaitForSeconds (1f);
		}
			


		HUDManager.enemySuccess.GetComponent<SuccessHUD> ().DisableThem ();
		HUDManager.playerSuccess.GetComponent<SuccessHUD> ().DisableThem ();
		StartCoroutine (GameObject.Find("Game").GetComponent<GameManager>().FlipENEMYButtons());
		yield return StartCoroutine (GameObject.Find("Game").GetComponent<GameManager>().FlipButtons());
			
		yield return null;
	}



	public IEnumerator VictoryAnimation(){
		// Requires animations.
		yield return null;
	}



	GameObject ReturnSuperiorSkill(GameObject player, Skill playerSkill, GameObject enemy, Skill enemySkill){
		int outcome = counters [(int)playerSkill, (int)enemySkill];
		if (outcome == 2) {
			return enemy;
		} else if (outcome == 1) {
			return player;
		} else {
			if (player.GetComponent<Character> ().agility > enemy.GetComponent<Character> ().agility) {
				return player;
			} else {
				return enemy;
			}
		}
	}



	IEnumerator EXECUTE(GameObject winner){
		ChoiceMenu.selectedPlayerCharacter = null;
		ChoiceMenu.selectedEnemyCharacter = null;
		ChoiceMenu.isEnemySelected = false;
		ChoiceMenu.isPlayerSelected = false;
		advantage = 50;

		yield return StartCoroutine (winner.GetComponent<Position> ().MovePosition (-2));

		GameObject other;
		int change;
		string direction;
		if (winner == GameManager.player) {
			other = GameManager.enemy;
		} else {
			other = GameManager.player;
		}
		if (winner.GetComponent<Position> ().position > other.GetComponent<Position> ().position) {
			direction = "left";
			change = winner.GetComponent<Position> ().position - other.GetComponent<Position> ().position;
		} else {
			direction = "right";
			change = other.GetComponent<Position> ().position - winner.GetComponent<Position> ().position;
		}

		yield return StartCoroutine (winner.GetComponent<Position> ().MovePosition (change));
		StartCoroutine (winner.GetComponent<Animations> ().PlayAnimation ("attack"));
		yield return new WaitForSeconds (winner.GetComponent<Animations> ().ReturnAnimationDuration ("attack"));

		yield return null;

		float moveDuration = 2f;
		Vector3 currentPosition = other.transform.position;
		Vector3 targetPosition = new Vector3 (0f, 0f, 0f);
		if (direction == "right") {
			Quaternion newRot = Quaternion.Euler (0f, gameObject.transform.rotation.y, 35f);
			other.transform.rotation = newRot;
			targetPosition = new Vector3 (30f, 10f, -1f);
		} else {
			Quaternion newRot = Quaternion.Euler (0f, gameObject.transform.rotation.y, 35f);
			other.transform.rotation = newRot;
			targetPosition = new Vector3 (-30f, 10f, -1f);
		}
		float time = 0f;

		while (time < moveDuration) {
			other.transform.position = Vector3.Lerp (currentPosition, targetPosition, time / moveDuration);
			time += Time.deltaTime;
			yield return new WaitForEndOfFrame ();
		}

		yield return null;








	}



	public void ExecuteSkill(GameObject entity, Skill skill, int phase){
		if (skill == Skill.Attack) {
			entity.GetComponent<Skills> ().Attack (phase);
		} else if (skill == Skill.Feign) {
			entity.GetComponent<Skills> ().Feign (phase);
		} else if (skill == Skill.Dodge) {
			entity.GetComponent<Skills> ().Dodge (phase);
		} else if (skill == Skill.Block) {
			entity.GetComponent<Skills> ().Block (phase);
		} else if (skill == Skill.Special) {
			entity.GetComponent<Skills> ().Special (phase);
		}
	}



	void ModifyAdvantage(bool playerVictory, GameObject winner, Skill skill, GameObject loser){
		int skillValue = winner.GetComponent<Character> ().ReturnSkillValue (skill);
		int strengthValue = winner.GetComponent<Character> ().strength;
		int enduranceValue = loser.GetComponent<Character> ().endurance;
		int totalValue;
		if(skill == Skill.Attack || skill == Skill.Special){
			totalValue = skillValue + strengthValue + winner.GetComponent<Skills>().power - enduranceValue + 100;
			winner.GetComponent<Skills> ().ResetPower ();
		} else {
			totalValue = skillValue + strengthValue - enduranceValue + 100;
		}

		float baseAdvantageModifier = 10;
		float finalAdvantageModifier = (baseAdvantageModifier / 100) * totalValue;
		int finalInt = Mathf.RoundToInt (finalAdvantageModifier);

		if (playerVictory) {
			StartCoroutine (HUDManager.advantageHUD.ShowText (finalInt, winner));
			advantage += finalInt;
		} else {
			StartCoroutine (HUDManager.advantageHUD.ShowText (finalInt, winner));
			advantage -= finalInt;
		}

		if (advantage > 100) {
			advantage = 100;
		}
		if (advantage < 0) {
			advantage = 0;
		}
	}



	public void SetCounters(){
		counters = new int[5, 5];
		counters [(int)Skill.Attack, (int)Skill.Attack] = 0;
		counters [(int)Skill.Attack, (int)Skill.Feign] = 1;
		counters [(int)Skill.Attack, (int)Skill.Dodge] = 2;
		counters [(int)Skill.Attack, (int)Skill.Special] = 1;
		counters [(int)Skill.Attack, (int)Skill.Block] = 2;

		counters [(int)Skill.Feign, (int)Skill.Attack] = 2;
		counters [(int)Skill.Feign, (int)Skill.Feign] = 0;
		counters [(int)Skill.Feign, (int)Skill.Dodge] = 1;
		counters [(int)Skill.Feign, (int)Skill.Special] = 2;
		counters [(int)Skill.Feign, (int)Skill.Block] = 1;

		counters [(int)Skill.Dodge, (int)Skill.Attack] = 1;
		counters [(int)Skill.Dodge, (int)Skill.Feign] = 2;
		counters [(int)Skill.Dodge, (int)Skill.Dodge] = 0;
		counters [(int)Skill.Dodge, (int)Skill.Special] = 1;
		counters [(int)Skill.Dodge, (int)Skill.Block] = 2;

		counters [(int)Skill.Special, (int)Skill.Attack] = 2;
		counters [(int)Skill.Special, (int)Skill.Feign] = 1;
		counters [(int)Skill.Special, (int)Skill.Dodge] = 2;
		counters [(int)Skill.Special, (int)Skill.Special] = 0;
		counters [(int)Skill.Special, (int)Skill.Block] = 1;

		counters [(int)Skill.Block, (int)Skill.Attack] = 1;
		counters [(int)Skill.Block, (int)Skill.Feign] = 2;
		counters [(int)Skill.Block, (int)Skill.Dodge] = 1;
		counters [(int)Skill.Block, (int)Skill.Special] = 2;
		counters [(int)Skill.Block, (int)Skill.Block] = 0;



		playAnimations = new bool[5, 5];
		playAnimations [(int)Skill.Attack, (int)Skill.Attack] = true;
		playAnimations [(int)Skill.Attack, (int)Skill.Feign] = true;
		playAnimations [(int)Skill.Attack, (int)Skill.Dodge] = true;
		playAnimations [(int)Skill.Attack, (int)Skill.Special] = true;
		playAnimations [(int)Skill.Attack, (int)Skill.Block] = true;

		playAnimations [(int)Skill.Feign, (int)Skill.Attack] = false;
		playAnimations [(int)Skill.Feign, (int)Skill.Feign] = true;
		playAnimations [(int)Skill.Feign, (int)Skill.Dodge] = true;
		playAnimations [(int)Skill.Feign, (int)Skill.Special] = false;
		playAnimations [(int)Skill.Feign, (int)Skill.Block] = true;

		playAnimations [(int)Skill.Dodge, (int)Skill.Attack] = true;
		playAnimations [(int)Skill.Dodge, (int)Skill.Feign] = true;
		playAnimations [(int)Skill.Dodge, (int)Skill.Dodge] = true;
		playAnimations [(int)Skill.Dodge, (int)Skill.Special] = true;
		playAnimations [(int)Skill.Dodge, (int)Skill.Block] = true;

		playAnimations [(int)Skill.Special, (int)Skill.Attack] = false;
		playAnimations [(int)Skill.Special, (int)Skill.Feign] = false;
		playAnimations [(int)Skill.Special, (int)Skill.Dodge] = true;
		playAnimations [(int)Skill.Special, (int)Skill.Special] = false;
		playAnimations [(int)Skill.Special, (int)Skill.Block] = false;

		playAnimations [(int)Skill.Block, (int)Skill.Attack] = true;
		playAnimations [(int)Skill.Block, (int)Skill.Feign] = true;
		playAnimations [(int)Skill.Block, (int)Skill.Dodge] = true;
		playAnimations [(int)Skill.Block, (int)Skill.Special] = false;
		playAnimations [(int)Skill.Block, (int)Skill.Block] = true;

	}
}


// Three possibilities.
// Melee attacks should bring a character in range at all times. Ranged attacks don't have to.
// This gives the possiblity to give characters penalties or bonusses IF they move. Stored Damage meter maybe???? 
// Block/dodge/feign moves don't have to bring a character in range either.
// Move - attack - Move 

// 2-3 combo's per character that result in a super move?
// You execute two attacks properly (that means without losing a roll!
// You get a massive bonus.

