﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class ChoiceMenu : MonoBehaviour {

	public Sprite RETURNTOTHIS;
	public static GameObject selectedPlayerCharacter;
	public static GameObject selectedEnemyCharacter;

	public List<GameObject> characters;

	public static bool isPlayerSelected;
	public GameObject playerImage;
	public GameObject playerName;
	public GameObject playerDescription;
	public GameObject playerStats;
	public List<GameObject> playerSkillSlots;
	public static bool isEnemySelected;
	public GameObject enemyImage;
	public GameObject enemyName;
	public GameObject enemyDescription;
	public GameObject enemyStats;
	public List<GameObject> enemySkillSlots;

	public Sprite emptySkillSlot;

	public TextMeshProUGUI difficultyText;
	public Slider diffSlider;
	public static int difficulty;

	void Start(){
		playerImage = transform.Find ("PlayerImage").gameObject;
		playerName = transform.Find ("PlayerName").gameObject;
		playerStats = transform.Find ("PlayerStats").gameObject;
		playerDescription = playerName.transform.Find ("PlayerDescription").gameObject;

		enemyImage = transform.Find ("EnemyImage").gameObject;
		enemyName = transform.Find ("EnemyName").gameObject;
		enemyStats = transform.Find ("EnemyStats").gameObject;
		enemyDescription = enemyName.transform.Find ("EnemyDescription").gameObject;

		difficultyText = GameObject.Find ("LEVEL").GetComponent<TextMeshProUGUI> ();
		diffSlider = GameObject.Find ("DIFFICULTY").GetComponent<Slider> ();

		for (int x = 1; x < 6; x++) {
			playerSkillSlots.Add (playerStats.transform.Find ("Skill" + x ).gameObject);
			playerSkillSlots [x -1].GetComponent<Image> ().sprite = RETURNTOTHIS;
			enemySkillSlots.Add (enemyStats.transform.Find ("Skill" + x ).gameObject);
			enemySkillSlots [x -1].GetComponent<Image> ().sprite = RETURNTOTHIS;
		}

		Slider slider;
		slider = playerStats.transform.Find ("Strength").GetComponent<Slider> ();
		slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = new Color(0f, 0f, 0f, 0f);
		slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = new Color(1f, 1f, 1f, 0f);
		slider.value = 0;
		slider = playerStats.transform.Find ("Agility").GetComponent<Slider> ();
		slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = new Color(0f, 0f, 0f, 0f);
		slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = new Color(1f, 1f, 1f, 0f);
		slider.value = 0;
		slider = playerStats.transform.Find ("Endurance").GetComponent<Slider> ();
		slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = new Color(0f, 0f, 0f, 0f);
		slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = new Color(1f, 1f, 1f, 0f);
		slider = enemyStats.transform.Find ("Strength").GetComponent<Slider> ();
		slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = new Color(0f, 0f, 0f, 0f);
		slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = new Color(1f, 1f, 1f, 0f);
		slider.value = 0;
		slider = enemyStats.transform.Find ("Agility").GetComponent<Slider> ();
		slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = new Color(0f, 0f, 0f, 0f);
		slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = new Color(1f, 1f, 1f, 0f);
		slider.value = 0;
		slider = enemyStats.transform.Find ("Endurance").GetComponent<Slider> ();
		slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = new Color(0f, 0f, 0f, 0f);
		slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = new Color(1f, 1f, 1f, 0f);

	}


	public void Update(){
		if (diffSlider.value <= 20) {
			difficulty = 8;
			difficultyText.text = "Chirp!";
			difficultyText.fontSize = 40;
		} else if (diffSlider.value <= 40) {
			difficulty = 6;
			difficultyText.text = "Twitter!";
			difficultyText.fontSize = 45;
		} else if (diffSlider.value <= 60) {
			difficulty = 4;
			difficultyText.text = "Squawk!";
			difficultyText.fontSize = 50;
		} else if (diffSlider.value <= 80) {
			difficulty = 2;
			difficultyText.text = "Cackle!";
			difficultyText.fontSize = 55;
		} else if (diffSlider.value <= 100) {
			difficulty = 1;
			difficultyText.text = "Shriek!";
			difficultyText.fontSize = 60;
		}
	}



	public void DisplayCharacter(int value){
		if (value > characters.Count - 1) {
			return;
		}
		Slider slider;
		if (!isPlayerSelected) {
			Character selectedCharacter = characters[value].GetComponent<Character>();
			playerImage.GetComponent<Image> ().sprite = selectedCharacter.portrait;
			playerName.GetComponent<TextMeshProUGUI> ().text = selectedCharacter.characterName;
			playerDescription.GetComponent<TextMeshProUGUI> ().text = selectedCharacter.characterDescription;

			slider = playerStats.transform.Find ("Strength").GetComponent<Slider> ();
			slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = selectedCharacter.secondaryColor;
			slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = selectedCharacter.primaryColor;
			slider.value = selectedCharacter.strength;
			slider = playerStats.transform.Find ("Agility").GetComponent<Slider> ();
			slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = selectedCharacter.secondaryColor;
			slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = selectedCharacter.primaryColor;
			slider.value = selectedCharacter.agility;
			slider = playerStats.transform.Find ("Endurance").GetComponent<Slider> ();
			slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = selectedCharacter.secondaryColor;
			slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = selectedCharacter.primaryColor;
			slider.value = selectedCharacter.endurance;

			for(int x = 0; x < 5; x++){
				playerSkillSlots [x].GetComponent<Image> ().sprite = selectedCharacter.skills [x];
				playerSkillSlots [x].transform.Find ("Name").gameObject.GetComponent<TextMeshProUGUI> ().text = selectedCharacter.skillNames [x];
				playerSkillSlots [x].transform.Find ("Description").gameObject.GetComponent<TextMeshProUGUI> ().text = selectedCharacter.skillDescriptions [x];
			}
		} else {
			Character selectedCharacter = characters[value].GetComponent<Character>();
			enemyImage.GetComponent<Image> ().sprite = selectedCharacter.GetComponent<Character> ().portrait;
			enemyName.GetComponent<TextMeshProUGUI> ().text = selectedCharacter.GetComponent<Character> ().characterName;
			enemyDescription.GetComponent<TextMeshProUGUI> ().text = selectedCharacter.characterDescription;

			slider = enemyStats.transform.Find ("Strength").GetComponent<Slider> ();
			slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = selectedCharacter.secondaryColor;
			slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = selectedCharacter.primaryColor;
			slider.value = selectedCharacter.strength;
			slider = enemyStats.transform.Find ("Agility").GetComponent<Slider> ();
			slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = selectedCharacter.secondaryColor;
			slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = selectedCharacter.primaryColor;
			slider.value = selectedCharacter.agility;
			slider = enemyStats.transform.Find ("Endurance").GetComponent<Slider> ();
			slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = selectedCharacter.secondaryColor;
			slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = selectedCharacter.primaryColor;
			slider.value = selectedCharacter.endurance;

			for(int x = 0; x < 5; x++){
				enemySkillSlots [x].GetComponent<Image> ().sprite = selectedCharacter.skills [x];
				enemySkillSlots [x].transform.Find ("Name").gameObject.GetComponent<TextMeshProUGUI> ().text = selectedCharacter.skillNames [x];
				enemySkillSlots [x].transform.Find ("Description").gameObject.GetComponent<TextMeshProUGUI> ().text = selectedCharacter.skillDescriptions [x];
			}
		}
	}

	public void HideCharacter(){
		Slider slider;
		if (!isPlayerSelected) {
			playerImage.GetComponent<Image> ().sprite = RETURNTOTHIS;
			playerName.GetComponent<TextMeshProUGUI> ().text = null;
			playerDescription.GetComponent<TextMeshProUGUI> ().text = null;

			slider = playerStats.transform.Find ("Strength").GetComponent<Slider> ();
			slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = new Color(0f, 0f, 0f, 0f);
			slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = new Color(1f, 1f, 1f, 0f);
			slider.value = 0;
			slider = playerStats.transform.Find ("Agility").GetComponent<Slider> ();
			slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = new Color(0f, 0f, 0f, 0f);
			slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = new Color(1f, 1f, 1f, 0f);
			slider.value = 0;
			slider = playerStats.transform.Find ("Endurance").GetComponent<Slider> ();
			slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = new Color(0f, 0f, 0f, 0f);
			slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = new Color(1f, 1f, 1f, 0f);
			slider.value = 0;
			for(int x = 0; x < 5; x++){
				playerSkillSlots [x].GetComponent<Image> ().sprite = RETURNTOTHIS;
				playerSkillSlots [x].transform.Find ("Name").gameObject.GetComponent<TextMeshProUGUI> ().text = null;
				playerSkillSlots [x].transform.Find ("Description").gameObject.GetComponent<TextMeshProUGUI> ().text = null;
			}
		} else {
			enemyImage.GetComponent<Image> ().sprite = RETURNTOTHIS;
			enemyName.GetComponent<TextMeshProUGUI> ().text = null;
			enemyDescription.GetComponent<TextMeshProUGUI> ().text = null;

			slider = enemyStats.transform.Find ("Strength").GetComponent<Slider> ();
			slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = new Color(0f, 0f, 0f, 0f);
			slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = new Color(1f, 1f, 1f, 0f);
			slider.value = 0;
			slider = enemyStats.transform.Find ("Agility").GetComponent<Slider> ();
			slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = new Color(0f, 0f, 0f, 0f);
			slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = new Color(1f, 1f, 1f, 0f);
			slider.value = 0;
			slider = enemyStats.transform.Find ("Endurance").GetComponent<Slider> ();
			slider.gameObject.transform.Find ("Background").gameObject.GetComponent<Image> ().color = new Color(0f, 0f, 0f, 0f);
			slider.gameObject.transform.Find ("Fill Area").gameObject.GetComponentInChildren<Image> ().color = new Color(1f, 1f, 1f, 0f);
			slider.value = 0;
			for(int x = 0; x < 5; x++){
				enemySkillSlots [x].GetComponent<Image> ().sprite = RETURNTOTHIS;
				enemySkillSlots [x].transform.Find ("Name").gameObject.GetComponent<TextMeshProUGUI> ().text = null;
				enemySkillSlots [x].transform.Find ("Description").gameObject.GetComponent<TextMeshProUGUI> ().text = null;
			}
		}
	}

	public void SelectCharacter(int x){
		if (x > characters.Count - 1) {
			return;
		}
		if (!isPlayerSelected) {
			selectedPlayerCharacter = characters [x];
			isPlayerSelected = true;
			StartCoroutine (ShowVersusSign ());
			DisplayCharacter (x);
		} else {
			selectedEnemyCharacter = characters [x];
			isEnemySelected = true;
			StartCoroutine (FinishSelection ());
		}
	}

	IEnumerator ShowVersusSign(){
		yield return null;
	}

	IEnumerator FinishSelection(){

		StopAllCoroutines ();
		SceneManager.LoadScene ("Game");
		yield return null;
	}


}
