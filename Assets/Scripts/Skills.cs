﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skills : MonoBehaviour {

	public int attackMoveBefore;
	public int attackMoveAfter;
	public int feignMoveBefore;
	public int feignMoveAfter;
	public int dodgeMoveBefore;
	public int dodgeMoveAfter;
	public int blockMoveBefore;
	public int blockMoveAfter;
	public int specialMoveBefore;
	public int specialMoveAfter;

	public int power;

	public GameObject projectile;

	public virtual void Attack(int value){
		
	}

	public virtual void Dodge(int value){

	}

	public virtual void Feign(int value){
		
	}

	public virtual void Block(int value){

	} 

	public virtual void Special(int value){

	}

	public void ResetPower(){
		bool isAlreadyZero;
		if (power == 0) {
			isAlreadyZero = true;
		} else {
			isAlreadyZero = false;
		}
		power = 0;
		requestPowerUpdate (0, isAlreadyZero);
	}

	public void IncreasePower(int value){
		power += value;
		if( power > 100){
			power = 100;
		}
		requestPowerUpdate (1, false);
	}

	public void requestPowerUpdate(int req, bool hide){
		GameObject gamehud = GameObject.Find ("GameHUD");
		if (!hide) {
			if (gameObject == GameManager.player) {
				gamehud.transform.Find ("PlayerPower").GetComponent<PowerHUD> ().UpdateMeter (req);
			} else {
				gamehud.transform.Find ("EnemyPower").GetComponent<PowerHUD> ().UpdateMeter (req);
			}
		}
	}

	public int ReturnDistanceBefore(GameObject entity, Skill skill){
		int distance = 0;
		if (skill == Skill.Attack) {
			distance = attackMoveBefore;
		} else if (skill == Skill.Feign) {
			distance = feignMoveBefore;
		} else if (skill == Skill.Dodge) {
			distance = dodgeMoveBefore;
		} else if (skill == Skill.Block) {
			distance = blockMoveBefore;
		} else if (skill == Skill.Special){
			distance = specialMoveBefore;
		}
		return distance;
	}

	public int ReturnDistanceAfter(GameObject entity, Skill skill){
		int distance = 0;
		if (skill == Skill.Attack) {
			distance = attackMoveAfter;
		} else if (skill == Skill.Feign) {
			distance = feignMoveAfter;
		} else if (skill == Skill.Dodge) {
			distance = dodgeMoveAfter;
		} else if (skill == Skill.Block) {
			distance = blockMoveAfter;
		} else if (skill == Skill.Special){
			distance = specialMoveAfter;
		}
		return distance;
	}
}
