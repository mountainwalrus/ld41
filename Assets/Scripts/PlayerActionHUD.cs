﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerActionHUD : MonoBehaviour {

	public List<Sprite> skills;
	public Sprite skillBack;
	public Sprite skillHighlight;
	public Sprite skillOpen;
	public List<Image> images;

	public int enemySprites;

	void Awake(){
		images = new List<Image> ();
		foreach (Image image in GetComponentsInChildren<Image>()) {
			images.Add (image);
		}
	}
		


	public void SetSprites(Character characterComponent){
		skills = characterComponent.skills;
		skillBack = characterComponent.skillBack;
		skillOpen = characterComponent.skillOpen;
		foreach (Image image in images) {
			image.sprite = skillBack;
		}

	}



	public IEnumerator FlipSkill(int seq){
		float rotationTarget;
		Image currentImage = images [seq];
		Sprite targetSprite; 
		if (currentImage.sprite == skillBack) {
			targetSprite = skillOpen;
			rotationTarget = 180f;
		} else {
			targetSprite = skillBack;
			rotationTarget = 0f;
		}
		Vector3 rotation = new Vector3 (0f, 360f * Time.deltaTime, 0f);
		bool isFlipped = false;
		while (!isFlipped) {
			currentImage.gameObject.transform.Rotate(rotation);
			if (currentImage.gameObject.transform.rotation.eulerAngles.y > 85) {
				currentImage.sprite = targetSprite;
			}
			if (currentImage.gameObject.transform.rotation.eulerAngles.y > 175) {
				Quaternion finalRotation = Quaternion.Euler(0f, rotationTarget, 0f);
				currentImage.gameObject.transform.rotation = finalRotation;
				isFlipped = true;
			}
			yield return null;
		}
	}
		


	public IEnumerator FlipSkillEnemy(int seq, Skill skill){
		float rotationTarget;
		Image currentImage = images [seq];
		Sprite targetSprite; 
		if (currentImage.sprite == skillBack) {
			targetSprite = skills[(int)skill];
			rotationTarget = 180f;
		} else {
			targetSprite = skillBack;
			rotationTarget = 0f;
		}
		Vector3 rotation = new Vector3 (0f, 240f * Time.deltaTime, 0f);
		bool isFlipped = false;
		while (!isFlipped) {
			currentImage.gameObject.transform.Rotate(rotation);
			if (currentImage.gameObject.transform.rotation.eulerAngles.y > 85) {
				currentImage.sprite = targetSprite;
			}
			if (currentImage.gameObject.transform.rotation.eulerAngles.y > 175) {
				Quaternion finalRotation = Quaternion.Euler(0f, rotationTarget, 0f);
				currentImage.gameObject.transform.rotation = finalRotation;
				isFlipped = true;
			}
			yield return null;
		}
	}

	public IEnumerator SetNewButton(Skill skill, int number){
		images [number].sprite = skills[(int)skill];
		yield return new WaitForSeconds (1);
	}
}
