﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuccessHUD : MonoBehaviour {

	GameObject theOne;
	public List<Image> images;
	public Sprite failure;
	public Sprite success;

	void Awake(){
		images = new List<Image> ();
		foreach (Image image in GetComponentsInChildren<Image>()) {
			images.Add (image);
			image.gameObject.SetActive (false);
		}
	}

	public void SetSprites(){
		if (gameObject.name == "EnemySuccess") {
			theOne = GameManager.enemy;
		} else {
			theOne = GameManager.player;
		}
		Character characterComponent = theOne.GetComponent<Character> ();
		failure = characterComponent.failure;
		success = characterComponent.success;
	}

	public void DisableThem(){
		foreach (Image image in images) {
			image.gameObject.SetActive (false);
		}
	}

	public void TriggerSuccess(int x){
		images [x].gameObject.SetActive (true);
		images [x].sprite = success;
	}

	public void TriggerFailure(int x){
		images [x].gameObject.SetActive (true);
		images [x].sprite = failure;
	}
}
