﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character3Skills : Skills {

	public override void Attack(int value){
		if (value == 0) {
			GameObject other;
			int change;
			if (gameObject == GameManager.player) {
				other = GameManager.enemy;
			} else {
				other = GameManager.player;
			}
			if (gameObject.GetComponent<Position> ().position > other.GetComponent<Position> ().position) {
				change = gameObject.GetComponent<Position> ().position - other.GetComponent<Position> ().position;
			} else {
				change = other.GetComponent<Position> ().position - gameObject.GetComponent<Position> ().position;
			}
			attackMoveBefore = change;
		}
	}
		
	public override void Dodge(int value){
		if (value == 1) {
			GetComponent<Character> ().strength += 5;
		}
	}

	public override void Feign(int value){

	}

	public override void Block(int value){
		if (value == 1) {
			GetComponent<Character> ().endurance += 5;
		}
	} 

	public override void Special(int value){
		if (value == 1) {
			if (gameObject == GameManager.player) {
				if (GameManager.enemy.GetComponent<Skills> ().power > 0) {
					power += GameManager.enemy.GetComponent<Skills> ().power;
					GameManager.enemy.GetComponent<Skills> ().ResetPower ();
				}
			} else {
				if (GameManager.player.GetComponent<Skills> ().power > 0) {
					power += GameManager.player.GetComponent<Skills> ().power;
					GameManager.player.GetComponent<Skills> ().ResetPower ();
				}
			}
		}
	}
}
