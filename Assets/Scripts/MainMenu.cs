﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	void Awake(){
		Screen.fullScreen = !Screen.fullScreen;
		Screen.SetResolution (1280, 720, false);
	}



	void Update(){
		if(Input.GetKeyUp("return")){
			StartCoroutine (StartGame ());
		}
		if(Input.GetKeyUp("space")){
			StartCoroutine (ShowInstructions ());
		}
	}

	IEnumerator StartGame(){
		SceneManager.LoadScene ("CharacterSelection");
		yield return null;
	}

	IEnumerator ShowInstructions(){
		SceneManager.LoadScene ("Instructions");
		yield return null;
	}

}
