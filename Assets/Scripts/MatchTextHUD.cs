﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MatchTextHUD : MonoBehaviour {

	TextMeshProUGUI textComponent;


	void Awake(){
		textComponent = GetComponent<TextMeshProUGUI> ();
	}


	public IEnumerator MatchCoolDown(){
		for (int x = 3; x > -1; x--) {
			if (x > 0) {
				textComponent.text = x.ToString ();
			} else {
				textComponent.text = "CHITTER!";
			}
			yield return new WaitForSeconds (1);
		}
		textComponent.text = null; // Maybe fade text while match starts?
	}
		
	public IEnumerator PickSkills(){
		textComponent.text = "CHOOSE YOUR SQUAWKS!";
		yield return new WaitForSeconds (2);
		textComponent.text = null;
	}

	public IEnumerator ShowDown(){
		textComponent.text = "RUFFLE SOME FEATHERS!";
		yield return new WaitForSeconds (2);
		textComponent.text = null;
	}

	public IEnumerator Fatality(){
		textComponent.text = "FA-TWEET-ITY!!!";
		yield return new WaitForSeconds (2);
	}

	public IEnumerator Victory(GameObject winner){
		if (winner == GameManager.player) {
			textComponent.text = GameManager.player.GetComponent<Character>().characterName + " Wins!";
		} else {
			textComponent.text = GameManager.enemy.GetComponent<Character>().characterName + " Wins!";
		}
		yield return new WaitForSeconds (4);
	}
}
