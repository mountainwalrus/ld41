﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class Character1Skills : Skills {

	public SkeletonAnimation skeletonAnimation;
	public Spine.AnimationState spineAnimationState;
	public Spine.Skeleton skeleton;

	public override void Attack(int value){
		if (value == 0) {
			GameObject other;
			int change;
			if (gameObject == GameManager.player) {
				other = GameManager.enemy;
			} else {
				other = GameManager.player;
			}
			if (gameObject.GetComponent<Position> ().position > other.GetComponent<Position> ().position) {
				change = gameObject.GetComponent<Position> ().position - other.GetComponent<Position> ().position;
			} else {
				change = other.GetComponent<Position> ().position - gameObject.GetComponent<Position> ().position;
			}
			attackMoveBefore = change;
		}
	}
		
	public override void Dodge(int value){

	}

	public override void Feign(int value){
		if (value == 1) {
			IncreasePower (35);
		}
	}

	public override void Block(int value){

	} 

	public override void Special(int value){
		if (value == 0) {
			StartCoroutine (gameObject.GetComponent<Position> ().MovePosition (-1));
		}
		if (value == 1) {

			GameObject other;
			int change;
			if (gameObject == GameManager.player) {
				other = GameManager.enemy;
			} else {
				other = GameManager.player;
			}
			if (gameObject.GetComponent<Position> ().position > other.GetComponent<Position> ().position) {
				change = gameObject.GetComponent<Position> ().position - other.GetComponent<Position> ().position;
			} else {
				change = other.GetComponent<Position> ().position - gameObject.GetComponent<Position> ().position;
			}

			StartCoroutine (FireProjectile (other));
			IncreasePower (25);

		}
	}

	IEnumerator FireProjectile(GameObject target){

		yield return new WaitForSeconds (gameObject.GetComponent<Animations> ().ReturnAnimationDuration ("special") / 2);
		GameObject shotProjectile = Instantiate (projectile, transform);
		skeletonAnimation = shotProjectile.GetComponent<SkeletonAnimation>();
		spineAnimationState = skeletonAnimation.AnimationState;
		skeleton = skeletonAnimation.Skeleton;

		Vector3 startPosition = new Vector3 (1.9f, 1.4f, -1f);
		shotProjectile.transform.localPosition = startPosition;
		Vector3 cheatPosition = new Vector3 (shotProjectile.transform.position.x, shotProjectile.transform.position.y, -8f);
		shotProjectile.transform.position = cheatPosition;


		float moveDuration = (skeletonAnimation.Skeleton.Data.FindAnimation ("travel").duration + skeletonAnimation.Skeleton.Data.FindAnimation ("spawn").duration) /4;
		float halftime = skeletonAnimation.Skeleton.Data.FindAnimation ("spawn").duration /4;
		Vector3 currentPosition = shotProjectile.transform.position;
		Vector3 targetPosition = new Vector3 (target.transform.position.x, currentPosition.y, currentPosition.z);
		float time = 0f;
		spineAnimationState.SetAnimation (0, "spawn", false).timeScale = 4f;

		while (time < moveDuration) {
			if (time > halftime) {
				spineAnimationState.SetAnimation (0, "travel", false).timeScale = 4f;
			}
			shotProjectile.transform.position = Vector3.Lerp (currentPosition, targetPosition, time / moveDuration);
			time += Time.deltaTime;
			yield return new WaitForEndOfFrame ();
		}

		spineAnimationState.SetAnimation (0, "impact", false);
		yield return new WaitForSeconds (skeletonAnimation.Skeleton.Data.FindAnimation ("impact").duration);
		Destroy (shotProjectile);

		yield return null;
	}
}
