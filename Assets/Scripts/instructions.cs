﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class instructions : MonoBehaviour {

	void Update(){
		if(Input.GetKeyUp("return")){
			StartCoroutine (StartGame ());
		}
	}

	IEnumerator StartGame(){
		SceneManager.LoadScene ("CharacterSelection");
		yield return null;
	}

}
