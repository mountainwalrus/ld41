﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {

	bool playerInputFinished;
	public List<Skill> queuedActions;


	public IEnumerator ProcessPlayerInput(){
		queuedActions = new List<Skill> ();
		playerInputFinished = false;
		while (!playerInputFinished) {
			Coroutine routine = StartCoroutine (AcceptPlayerInput());
			yield return new WaitForSeconds (0);
			StopCoroutine (routine);
			if (queuedActions.Count >= 3) {
				playerInputFinished = true;
			}
		}
		yield return null;
	}

	public IEnumerator AcceptPlayerInput(){
		while (!playerInputFinished) {
			int value = queuedActions.Count;
			if (value <= 2) {
				if (Input.GetKeyDown (KeyCode.Q)) {
					queuedActions.Add(Skill.Attack);
					StartCoroutine(HUDManager.actionOptionHUD.AppearAndFadeColor(Skill.Attack));
					yield return StartCoroutine(HUDManager.playerActionHUD.SetNewButton (Skill.Attack, value));
				} else if (Input.GetKeyDown (KeyCode.W)) {
					queuedActions.Add(Skill.Feign);
					StartCoroutine(HUDManager.actionOptionHUD.AppearAndFadeColor(Skill.Feign));
					yield return StartCoroutine(HUDManager.playerActionHUD.SetNewButton (Skill.Feign, value));
				} else if (Input.GetKeyDown (KeyCode.E)) {
					queuedActions.Add(Skill.Dodge);
					StartCoroutine(HUDManager.actionOptionHUD.AppearAndFadeColor(Skill.Dodge));
					yield return StartCoroutine (HUDManager.playerActionHUD.SetNewButton (Skill.Dodge, value));
				} else if (Input.GetKeyDown (KeyCode.R)) {
					queuedActions.Add(Skill.Block);
					StartCoroutine(HUDManager.actionOptionHUD.AppearAndFadeColor(Skill.Block));
					yield return StartCoroutine (HUDManager.playerActionHUD.SetNewButton (Skill.Block, value));
				} else if (Input.GetKeyDown (KeyCode.T)) {
					queuedActions.Add(Skill.Special);
					StartCoroutine(HUDManager.actionOptionHUD.AppearAndFadeColor(Skill.Special));
					yield return StartCoroutine (HUDManager.playerActionHUD.SetNewButton (Skill.Special, value));
				}
			}
			yield return new WaitForSeconds(1);
		}
	}
}
