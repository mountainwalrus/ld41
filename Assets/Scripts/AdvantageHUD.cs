﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AdvantageHUD : MonoBehaviour {

	GameObject text;

	void Start(){
		text = transform.Find ("Counter").gameObject;
		text.SetActive (false);
	}

	public void UpdateAdvantageHUD(){
		Slider sliderComponent = GetComponent<Slider> ();
		sliderComponent.value = CombatManager.advantage;
	}

	public IEnumerator ShowText(int value, GameObject winner){
		
		text.SetActive (true);
		if (winner == GameManager.player) {
			text.GetComponent<TextMeshProUGUI> ().text = value.ToString ();
		} else {
			text.GetComponent<TextMeshProUGUI> ().text = value.ToString ();
		}
		text.GetComponent<TextMeshProUGUI> ().color = winner.GetComponent<Character> ().primaryColor;
		yield return new WaitForSeconds (1.5f);
		text.SetActive (false);
	}
}
