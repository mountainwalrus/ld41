﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CharacterText : MonoBehaviour {

	public GameObject owner;

	public Color color;


	void Start(){
		gameObject.SetActive(false);
	}

	void Update(){
		if (owner == GameManager.player) {
			if (owner.GetComponent<Position>().position > GameManager.enemy.GetComponent<Position> ().position) {
				Quaternion finalRotation = Quaternion.Euler(0f, 0f, -45f);
				gameObject.transform.rotation = finalRotation;
				Vector3 newPosition = new Vector3 (owner.transform.position.x + 1f, 0.5f, -2f);
				transform.position = newPosition;
			} else {
				Quaternion finalRotation = Quaternion.Euler(0f, 0f, 45f);
				gameObject.transform.rotation = finalRotation;
				Vector3 newPosition = new Vector3 (owner.transform.position.x - 1f, 0.5f, -2f);
				transform.position = newPosition;
			}
		} else {
			if (owner.GetComponent<Position>().position > GameManager.player.GetComponent<Position> ().position) {
				Quaternion finalRotation = Quaternion.Euler(0f, 0f, -45f);
				gameObject.transform.rotation = finalRotation;
				Vector3 newPosition = new Vector3 (owner.transform.position.x + 1f, 0.5f, -2f);
				transform.position = newPosition;
			} else {
				Quaternion finalRotation = Quaternion.Euler(0f, 0f, 45f);
				gameObject.transform.rotation = finalRotation;
				Vector3 newPosition = new Vector3 (owner.transform.position.x - 1f, 0.5f, -2f);
				transform.position = newPosition;
			}
		}
	}

	public IEnumerator DisplayText(string text, bool isWinner){
		if (isWinner) {
			GetComponent<TextMeshPro> ().fontSize = 8;
		} else {
			GetComponent<TextMeshPro> ().fontSize = 4;
		}
		gameObject.SetActive (true);
		GetComponent<TextMeshPro> ().color = color;
		GetComponent<TextMeshPro> ().text = text + "!";



		yield return new WaitForSeconds(2f);

		gameObject.SetActive (false);
	}

}
