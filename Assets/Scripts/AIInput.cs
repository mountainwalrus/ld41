﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIInput : MonoBehaviour {

	int attackCounter;
	int feignCounter;
	int dodgeCounter;
	int blockCounter;
	int specialCounter;

	int attackCounter0;
	int feignCounter0;
	int dodgeCounter0;
	int blockCounter0;
	int specialCounter0;

	int attackCounter1;
	int feignCounter1;
	int dodgeCounter1;
	int blockCounter1;
	int specialCounter1;

	int attackCounter2;
	int feignCounter2;
	int dodgeCounter2;
	int blockCounter2;
	int specialCounter2;

	public int difficulty;

	Dictionary<Skill, int> skillRollTable;

	public Skill ReturnEnemySkillChoise(Skill skill, int seq){

		ProcessPlayerChoice (skill, seq);
		skillRollTable = new Dictionary<Skill, int> ();
		int chosenNumber = 0;
		int random = 0;



		if (seq == 0) {
			skillRollTable.Add (Skill.Attack, 1 + attackCounter + attackCounter0);
			skillRollTable.Add (Skill.Feign, skillRollTable [Skill.Attack] + difficulty + feignCounter + feignCounter0);
			skillRollTable.Add (Skill.Dodge, skillRollTable [Skill.Feign] + difficulty + dodgeCounter + dodgeCounter0);
			skillRollTable.Add (Skill.Block, skillRollTable [Skill.Dodge] + difficulty + blockCounter + blockCounter0);
			skillRollTable.Add (Skill.Special, skillRollTable [Skill.Block] + difficulty + specialCounter + specialCounter0);

			int finalNumber = skillRollTable [Skill.Special] +1;
			chosenNumber = Random.Range (1, finalNumber);
			random = Random.Range (1, 3);

		} else if (seq == 1) {
			skillRollTable.Add (Skill.Attack, 1 + attackCounter + attackCounter1);
			skillRollTable.Add (Skill.Feign, skillRollTable [Skill.Attack] + difficulty + feignCounter + feignCounter1);
			skillRollTable.Add (Skill.Dodge, skillRollTable [Skill.Feign] + difficulty + dodgeCounter + dodgeCounter1);
			skillRollTable.Add (Skill.Block, skillRollTable [Skill.Dodge] + difficulty + blockCounter + blockCounter1);
			skillRollTable.Add (Skill.Special, skillRollTable [Skill.Block] + difficulty + specialCounter + specialCounter1);

			int finalNumber = skillRollTable [Skill.Special] +1;
			chosenNumber = Random.Range (1, finalNumber);
			random = Random.Range (1, 3);

		} else if (seq == 2) {
			skillRollTable.Add (Skill.Attack, 1 + attackCounter + attackCounter1);
			skillRollTable.Add (Skill.Feign, skillRollTable [Skill.Attack] + 1 + feignCounter + feignCounter2);
			skillRollTable.Add (Skill.Dodge, skillRollTable [Skill.Feign] + 1 + dodgeCounter + dodgeCounter2);
			skillRollTable.Add (Skill.Block, skillRollTable [Skill.Dodge] + 1 + blockCounter + blockCounter2);
			skillRollTable.Add (Skill.Special, skillRollTable [Skill.Block] + 1 + specialCounter + specialCounter2);

			int finalNumber = skillRollTable [Skill.Special] +1;
			chosenNumber = Random.Range (1, finalNumber);
			random = Random.Range (1, 3);
		}
			
		if (chosenNumber <= skillRollTable [Skill.Attack]) {
			if (random == 1) {
				return Skill.Dodge;
			} else if (random == 2) {
				return Skill.Block;
			}
		} else if (chosenNumber <= skillRollTable [Skill.Feign]) {
			if (random == 1) {
				return Skill.Attack;
			} else if (random == 2) {
				return Skill.Special;
			}
		} else if (chosenNumber <= skillRollTable [Skill.Dodge]) {
			if (random == 1) {
				return Skill.Feign;
			} else if (random == 2) {
				return Skill.Block;
			}
		} else if (chosenNumber <= skillRollTable [Skill.Block]) {
			if (random == 1) {
				return Skill.Feign;
			} else if (random == 2) {
				return Skill.Special;
			}
		} else {
			if (random == 1) {
				return Skill.Attack;
			} else if (random == 2) {
				return Skill.Dodge;
			}
		}





		return Skill.Special; // Delete this.
	}
		
	void ProcessPlayerChoice(Skill skill, int seq){
		if (skill == Skill.Attack) {
			attackCounter += 1;
		} else if (skill == Skill.Feign) {
			feignCounter += 1;
		} else if (skill == Skill.Dodge) {
			dodgeCounter += 1;
		} else if (skill == Skill.Block) {
			blockCounter += 1;
		} else if (skill == Skill.Special) {
			specialCounter += 1;
		}

		if (seq == 0) {
			if (skill == Skill.Attack) {
				attackCounter0 += 1;
			} else if (skill == Skill.Feign) {
				feignCounter0 += 1;
			} else if (skill == Skill.Dodge) {
				dodgeCounter0 += 1;
			} else if (skill == Skill.Block) {
				blockCounter0 += 1;
			} else if (skill == Skill.Special) {
				specialCounter0 += 1;
			}
		} else if (seq == 1) {
			if (skill == Skill.Attack) {
				attackCounter1 += 1;
			} else if (skill == Skill.Feign) {
				feignCounter1 += 1;
			} else if (skill == Skill.Dodge) {
				dodgeCounter1 += 1;
			} else if (skill == Skill.Block) {
				blockCounter1 += 1;
			} else if (skill == Skill.Special) {
				specialCounter1 += 1;
			}
		} else if (seq == 2) {
			if (skill == Skill.Attack) {
				attackCounter2 += 1;
			} else if (skill == Skill.Feign) {
				feignCounter2 += 1;
			} else if (skill == Skill.Dodge) {
				dodgeCounter2 += 1;
			} else if (skill == Skill.Block) {
				blockCounter2 += 1;
			} else if (skill == Skill.Special) {
				specialCounter2 += 1;
			}
		}
	}
}
