﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HUDManager : MonoBehaviour {

	public static PlayerActionHUD playerActionHUD;
	public static PlayerActionHUD enemyActionHUD;
	public static ActionOptionHUD actionOptionHUD;
	public static SuccessHUD playerSuccess;
	public static SuccessHUD enemySuccess;
	public static AdvantageHUD advantageHUD;
	public static MatchTextHUD matchTextHUD;

	public static GameObject playerTWEET;
	public static GameObject enemyTWEET;

	void Awake(){
		playerActionHUD = gameObject.transform.Find("PlayerActionHUD").GetComponent<PlayerActionHUD> ();
		playerSuccess = gameObject.transform.Find ("PlayerSuccess").GetComponent<SuccessHUD> ();
		enemyActionHUD = gameObject.transform.Find ("EnemyActionHUD").GetComponent<PlayerActionHUD> ();
		enemySuccess = gameObject.transform.Find ("EnemySuccess").GetComponent<SuccessHUD> ();
		actionOptionHUD = GetComponentInChildren<ActionOptionHUD> ();
		advantageHUD = GetComponentInChildren<AdvantageHUD> ();
		matchTextHUD = GetComponentInChildren<MatchTextHUD> ();
		playerTWEET = GameObject.Find ("PlayerTweet");
		playerTWEET.SetActive (false);
		enemyTWEET = GameObject.Find ("EnemyTweet");
		enemyTWEET.SetActive (false);
	}



	public static void InitializeHUD(){
		Character characterComponent = GameManager.player.GetComponent<Character> ();
		playerActionHUD.SetSprites (characterComponent);
		actionOptionHUD.SetSprites (characterComponent);
		characterComponent = GameManager.enemy.GetComponent<Character> ();
		enemyActionHUD.SetSprites (characterComponent);
		enemySuccess.SetSprites ();
		playerSuccess.SetSprites ();
		GameObject pp = GameObject.Find ("PLAYERPORTRAIT").gameObject;
		GameObject ep = GameObject.Find ("ENEMYPORTRAIT").gameObject;
		pp.GetComponent<Image> ().sprite = GameManager.player.GetComponent<Character> ().miniPortrait;
		ep.GetComponent<Image> ().sprite = GameManager.enemy.GetComponent<Character> ().miniPortrait;

		if (GameManager.player.GetComponent<Character> ().primaryColor != GameManager.enemy.GetComponent<Character> ().primaryColor) {
			GameObject.Find ("AdvantageHUD").transform.Find ("Background").GetComponent<Image> ().color = GameManager.enemy.GetComponent<Character> ().secondaryColor;
		} else {
			GameObject.Find ("AdvantageHUD").transform.Find ("Background").GetComponent<Image> ().color = GameManager.enemy.GetComponent<Character> ().primaryColor;
		}
		GameObject.Find ("AdvantageHUD").transform.Find ("Fill Area").GetComponentInChildren<Image> ().color = GameManager.player.GetComponent<Character> ().secondaryColor;
	}



	public IEnumerator DisplayTweet(GameObject entity, string text){
		GameObject textbox;
		if (entity == GameManager.player) {
			textbox = playerTWEET;
		} else {
			textbox = enemyTWEET;
		}

		textbox.SetActive (true);
		TextMeshProUGUI textC = textbox.GetComponentInChildren<TextMeshProUGUI> ();
		int x = 0;
		string str = "";

		while (x < text.Length) {
			str += text [x];
			textC.text = str;
			x++;
			yield return new WaitForSeconds (0.1f);
		}
		yield return new WaitForSeconds (8f);
		textC.text = null;
		textbox.SetActive (false);
	}
}
