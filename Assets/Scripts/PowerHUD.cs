﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PowerHUD : MonoBehaviour {

	GameObject entity;
	GameObject powerText;

	void Start(){
		powerText = transform.Find ("POWER").gameObject;
		powerText.SetActive (false);
		if(gameObject.name == "PlayerPower"){
			entity = GameManager.player;
		} else {
			entity = GameManager.enemy;
		}

		Character character = entity.GetComponent<Character> ();
		gameObject.transform.Find ("Background").GetComponent<Image> ().color = character.primaryColor;
		gameObject.transform.Find ("Fill Area").GetComponentInChildren<Image> ().color = character.secondaryColor;
	}
		

	public void UpdateMeter(int val){
		Skills skillComponent = entity.GetComponent<Skills> ();
		Slider sliderComponent = GetComponent<Slider> ();
		sliderComponent.value = skillComponent.power;
		StartCoroutine (AppearAndFadeColor (val));
	}



		public IEnumerator AppearAndFadeColor(int val){
		TextMeshProUGUI textComponent = powerText.GetComponent<TextMeshProUGUI> ();
		if (val == 0) {
			textComponent.text = "Drained!";
		} else {
			textComponent.text = "+POWER!";
		}
		powerText.SetActive (true);

		float duration = 1.5f;
		Color originalColor = new Color (textComponent.color.r, textComponent.color.g, textComponent.color.b, 0f);
		textComponent.color = originalColor;
		Color targetColor = new Color (textComponent.color.r, textComponent.color.g, textComponent.color.b, 1f);
		float time = 0f;

		while (time < duration) {
			textComponent.color = Color.Lerp (originalColor, targetColor, duration / time);
			time += Time.deltaTime;
			yield return new WaitForEndOfFrame ();
		}

		textComponent.gameObject.SetActive (false);
		yield return null;
	}
}
