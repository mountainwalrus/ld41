﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ActionOptionHUD : MonoBehaviour {

	public List<Sprite> skills;
	public Sprite skillBack;
	public Sprite skillHighlight;
	public Sprite skillOpen;

	public List<Image> images;
	public List<TextMeshProUGUI> texts;
	public List<TextMeshProUGUI> popups;

	void Awake(){
		images = new List<Image> ();
		foreach (Image imageComponent in GetComponentsInChildren<Image>()) {
			images.Add (imageComponent);
		}
		texts = new List<TextMeshProUGUI> ();

		foreach (Image image in images) {
			TextMeshProUGUI textComponent = image.transform.Find("Hotkey").GetComponent<TextMeshProUGUI> ();
			texts.Add (textComponent);
			//textComponent.color = ChoiceMenu.selectedPlayerCharacter.GetComponent<Character> ().primaryColor;
			textComponent.gameObject.SetActive (false);
		}	
		popups = new List<TextMeshProUGUI> ();
		foreach (Image image in images) {
			TextMeshProUGUI textComponent = image.transform.Find("Popup").GetComponent<TextMeshProUGUI> ();
			popups.Add (textComponent);
			textComponent.gameObject.SetActive (false);
		}
	}



	public void SetSprites(Character characterComponent){
		skills = characterComponent.skills;
		skillBack = characterComponent.skillBack;
		skillOpen = characterComponent.skillOpen;
		foreach (Image image in images) {
			image.sprite = skillBack;
		}
	}



	public IEnumerator FlipSkill(int seq){
		float rotationTarget;
		Image currentImage = images [seq];
		Sprite targetSprite; 
		if (currentImage.sprite == skillBack) {
			targetSprite = skills[seq];
			rotationTarget = 180f;
		} else {
			targetSprite = skillBack;
			rotationTarget = 0f;
		}
		Vector3 rotation = new Vector3 (0f, 600f * Time.deltaTime, 0f);
		bool isFlipped = false;
		while (!isFlipped) {
			currentImage.gameObject.transform.Rotate(rotation);
			if (currentImage.gameObject.transform.rotation.eulerAngles.y > 85) {
				currentImage.sprite = targetSprite;
			}
			if (currentImage.gameObject.transform.rotation.eulerAngles.y > 175) {
				Quaternion finalRotation = Quaternion.Euler(0f, rotationTarget, 0f);
				currentImage.gameObject.transform.rotation = finalRotation;
				isFlipped = true;
			}
			yield return null;
		}
	}



	public void ToggleButtonsON(){
		for (int x = 0; x < 5; x++) {
			texts [x].gameObject.SetActive (true);
		}
	}

	public void ToggleButtonsOFF(){
		for (int x = 0; x < 5; x++) {
			texts [x].gameObject.SetActive (false);
		}
	}

	public IEnumerator AppearAndFadeColor(Skill skill){
		TextMeshProUGUI textComponent;

		if (skill == Skill.Attack) {
			textComponent = popups [0];
		} else if (skill == Skill.Feign) {
			textComponent = popups [1];
		} else if (skill == Skill.Dodge) {
			textComponent = popups [2];
		} else if (skill == Skill.Block) {
			textComponent = popups [3];
		} else {
			textComponent = popups [4];
		}

		textComponent.gameObject.SetActive (true);

		float duration = 0.5f;
		Color originalColor = new Color (textComponent.color.r, textComponent.color.g, textComponent.color.b, 0f);
		textComponent.color = originalColor;
		Color targetColor = new Color (textComponent.color.r, textComponent.color.g, textComponent.color.b, 1f);
		float time = 0f;

		while (time < duration) {
			textComponent.color = Color.Lerp (originalColor, targetColor, duration / time);
			time += Time.deltaTime;
			yield return new WaitForEndOfFrame ();
		}
			
		textComponent.gameObject.SetActive (false);
		yield return null;
	}
}
