﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Skill {Attack, Feign, Dodge, Block, Special, None}

public class GameManager : MonoBehaviour {

	public static GameObject player;
	public static GameObject enemy;
	public GameObject playerText;
	public GameObject enemyText;

	public GameObject textprefab;
	bool isMatchActive = true;

	public GameObject P1Prefab;
	public GameObject CPUPrefab;

	void Start(){
		
		StartCoroutine (MatchSequence());
		//foreach (Skill type in System.Enum.GetValues(typeof(Skill))) {
			//Debug.Log(type);
		//}
	}



	IEnumerator MatchSequence(){
		GetComponent<CombatManager> ().SetCounters ();
		SpawnPlayer ();
		SpawnEnemy ();
		SetInitialRotation ();
		SetArena ();
		SetDifficulty ();

		HUDManager.InitializeHUD ();
		HUDManager.advantageHUD.UpdateAdvantageHUD();








		yield return StartCoroutine (HUDManager.matchTextHUD.MatchCoolDown ());
		while (isMatchActive) {
			yield return StartCoroutine (StartQueueTurn ());
			yield return StartCoroutine (RequestPlayerInput ());
			yield return StartCoroutine (ExecuteActions ());
			if(isMatchActive){
				yield return new WaitForSeconds (2); // This will be deleted.
			}
		}	

		yield return StartCoroutine (GetComponent<CombatManager> ().VictoryAnimation ());
	}
		


	public void SetDifficulty(){
		int difficulty = ChoiceMenu.difficulty;
		enemy.GetComponent<AIInput> ().difficulty = difficulty;
		if (difficulty == 8) {
			enemy.GetComponent<Character> ().endurance -= 10;
			enemy.GetComponent<Character> ().strength -= 10;
			enemy.GetComponent<Character> ().agility -= 10;
			enemy.GetComponent<Character> ().attackPower -= 10;
			enemy.GetComponent<Character> ().feignPower -= 10;
			enemy.GetComponent<Character> ().dodgePower -= 10;
			enemy.GetComponent<Character> ().blockPower -= 10;
			enemy.GetComponent<Character> ().specialPower -= 10;
		} else if (difficulty == 6) {
			enemy.GetComponent<Character> ().endurance -= 5;
			enemy.GetComponent<Character> ().strength -= 5;
			enemy.GetComponent<Character> ().agility -= 5;
			enemy.GetComponent<Character> ().attackPower -= 5;
			enemy.GetComponent<Character> ().feignPower -= 5;
			enemy.GetComponent<Character> ().dodgePower -= 5;
			enemy.GetComponent<Character> ().blockPower -= 5;
			enemy.GetComponent<Character> ().specialPower -= 5;
		} else if (difficulty == 4) {
			enemy.GetComponent<Character> ().agility -= 5;
		} else if (difficulty == 2) {
			enemy.GetComponent<Character> ().endurance += 5;
			enemy.GetComponent<Character> ().strength += 5;
			enemy.GetComponent<Character> ().agility += 5;
			enemy.GetComponent<Character> ().attackPower += 5;
			enemy.GetComponent<Character> ().feignPower += 5;
			enemy.GetComponent<Character> ().dodgePower += 5;
			enemy.GetComponent<Character> ().blockPower += 5;
			enemy.GetComponent<Character> ().specialPower += 5;
		} else if (difficulty == 1) {
			enemy.GetComponent<Character> ().endurance += 10;
			enemy.GetComponent<Character> ().strength += 10;
			enemy.GetComponent<Character> ().agility += 10;
			enemy.GetComponent<Character> ().attackPower += 10;
			enemy.GetComponent<Character> ().feignPower += 10;
			enemy.GetComponent<Character> ().dodgePower += 10;
			enemy.GetComponent<Character> ().blockPower += 10;
			enemy.GetComponent<Character> ().specialPower += 10;
		}
	}
		

	IEnumerator StartQueueTurn(){
		int picker = Random.Range(1, 3);
		if (picker == 1) {
			StartCoroutine (GameObject.Find ("GameHUD").GetComponent<HUDManager> ().DisplayTweet (player, player.GetComponent<Character>().tweets[Random.Range(0, player.GetComponent<Character>().tweets.Count - 1)]));
		} else {
			StartCoroutine (GameObject.Find ("GameHUD").GetComponent<HUDManager> ().DisplayTweet (enemy, enemy.GetComponent<Character>().tweets[Random.Range(0, enemy.GetComponent<Character>().tweets.Count - 1)]));
		}

		StartCoroutine(FlipSkills ());
		//StartCoroutine (FlipENEMYButtons ());
		yield return StartCoroutine(FlipButtons ());
		yield return StartCoroutine (HUDManager.matchTextHUD.PickSkills ());
		HUDManager.actionOptionHUD.ToggleButtonsON ();
	}

	IEnumerator RequestPlayerInput(){
		yield return StartCoroutine (player.GetComponent<PlayerInput> ().ProcessPlayerInput ());
		yield return new WaitForSeconds (0.5f);
		StartCoroutine (FlipSkills ());
		HUDManager.actionOptionHUD.ToggleButtonsOFF ();
		yield return StartCoroutine (HUDManager.matchTextHUD.ShowDown ());
	}

	IEnumerator ExecuteActions(){
		yield return StartCoroutine (GetComponent<CombatManager>().ExecuteSkillSequence());
	}



	public IEnumerator FlipButtons(){
		for (int x = 0; x < 3; x++) {
			yield return StartCoroutine (HUDManager.playerActionHUD.FlipSkill (x));
		}
	}

	public IEnumerator FlipENEMYButtons(){
		for (int x = 0; x < 3; x++) {
			yield return StartCoroutine (HUDManager.enemyActionHUD.FlipSkill (x));
		}
	}


	IEnumerator FlipSkills(){
		for (int x = 0; x < 5; x++) {
			yield return StartCoroutine (HUDManager.actionOptionHUD.FlipSkill (x));
		}
	}



	public void SetMatchAsInactive(){
		isMatchActive = false;
	}



	void SpawnPlayer(){
		player = Instantiate (ChoiceMenu.selectedPlayerCharacter, transform);
		GameObject marker = Instantiate (P1Prefab, transform);
		marker.GetComponent<Buttons> ().AssignObject (player);
		playerText = Instantiate (textprefab, transform);
		playerText.GetComponent<CharacterText> ().color = player.GetComponent<Character> ().secondaryColor;
		playerText.GetComponent<CharacterText> ().owner = player;
		player.name = "Player";
		player.AddComponent<PlayerInput> ();
		Position positionComponent = player.GetComponent<Position> ();
		positionComponent.SetPosition (-1);
	}

	void SpawnEnemy(){
		enemy = Instantiate (ChoiceMenu.selectedEnemyCharacter, transform);
		GameObject marker = Instantiate (CPUPrefab, transform);
		marker.GetComponent<Buttons> ().AssignObject (enemy);
		enemyText = Instantiate (textprefab, transform);
		enemyText.GetComponent<CharacterText> ().owner = enemy;
		if (enemy.GetComponent<Character> ().secondaryColor != player.GetComponent<Character> ().secondaryColor) {
			enemyText.GetComponent<CharacterText> ().color = enemy.GetComponent<Character> ().secondaryColor;
		} else {
			enemyText.GetComponent<CharacterText> ().color = enemy.GetComponent<Character> ().secondaryColor;
		}
		enemy.name = "Enemy";
		enemy.AddComponent<AIInput> ();
		Position positionComponent = enemy.GetComponent<Position> ();
		positionComponent.SetPosition (1);
	}

	void SetInitialRotation(){
		Position positionComponent = player.GetComponent<Position> ();
		positionComponent.SetRotation ();
		positionComponent = enemy.GetComponent<Position> ();
		positionComponent.SetRotation ();
	}
	void SetArena(){
		//GameObject arena = GameObject.Find ("Arena");
		//GameObject background = arena.transform.Find ("Background").gameObject;
		//background.GetComponent<Image> ().sprite = ArenaMenu.selectedArena.GetComponent<Character> ().arenaBackground;
		//GameObject foreground = arena.transform.Find ("Foreground").gameObject;
		//foreground.GetComponent<Image> ().sprite = ArenaMenu.selectedArena.GetComponent<Character> ().arenaForeground;
	}
}
