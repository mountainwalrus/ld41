﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

	public string characterName;
	public string characterDescription;
	public Sprite portrait;
	public Sprite miniPortrait;
	public Color primaryColor;
	public Color secondaryColor;

	public List<Sprite> skills;
	public List<string> skillNames;
	public List<string> skillDescriptions;

	public List<string> tweets;

	public Sprite skillBack;
	public Sprite skillOpen;
	public Sprite success;
	public Sprite failure;

	public Dictionary<string, float> speedAdjustments;
	public float forwardSpeed;
	public float backSpeed;
	public float hitSpeed;
	public float attackSpeed;
	public float feignSpeed;
	public float dodgeSpeed;
	public float blockSpeed;
	public float specialSpeed;

	public int endurance;
	public int strength;
	public int agility;

	public int attackPower;
	public int feignPower;
	public int dodgePower;
	public int blockPower;
	public int specialPower;

	public Sprite CPU;
	public Sprite P1;

	void Awake(){
		speedAdjustments = new Dictionary<string, float> ();
		speedAdjustments.Add ("forward", forwardSpeed);
		speedAdjustments.Add ("back", backSpeed);
		speedAdjustments.Add ("hit", hitSpeed);
		speedAdjustments.Add ("attack", attackSpeed);
		speedAdjustments.Add ("feign", feignSpeed);
		speedAdjustments.Add ("dodge", dodgeSpeed);
		speedAdjustments.Add ("block", blockSpeed);
		speedAdjustments.Add ("special", specialSpeed);
	}

	public int ReturnSkillValue(Skill skillUsed){
		if (skillUsed == Skill.Attack) {
			return attackPower;
		} else if (skillUsed == Skill.Feign) {
			return feignPower;
		} else if (skillUsed == Skill.Dodge) {
			return dodgePower;
		} else if (skillUsed == Skill.Block) {
			return blockPower;
		} else if (skillUsed == Skill.Special) {
			return specialPower;
		} else {
			return 0;
		} 
	}
}
