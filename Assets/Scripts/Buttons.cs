﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Buttons : MonoBehaviour {

	GameObject assignedObject;

	void Update () {
		Vector3 ownerPosition = assignedObject.transform.position;
		Vector3 kittenPosition = new Vector3 (ownerPosition.x -0.2f, ownerPosition.y - 0.6f, ownerPosition.z);
		transform.position = kittenPosition;
	}

	public void AssignObject(GameObject owner){
		assignedObject = owner;
		GetComponent<TextMeshPro> ().color = owner.GetComponent<Character> ().secondaryColor;
	}
}
