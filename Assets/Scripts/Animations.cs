﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class Animations : MonoBehaviour {

	public SkeletonAnimation skeletonAnimation;
	public Spine.AnimationState spineAnimationState;
	public Spine.Skeleton skeleton;

	void Start () {
		skeletonAnimation = GetComponent<SkeletonAnimation>();
		spineAnimationState = skeletonAnimation.AnimationState;
		skeleton = skeletonAnimation.Skeleton;

			
		StartCoroutine (IdleAnimation ());
	}

	public void SetIdleAnimation(){

	}

	public IEnumerator IdleAnimation(){
		spineAnimationState.SetAnimation (0, "idle", true);
		yield break;
		//yield return new WaitForSeconds (1);
	}

	public IEnumerator PlayAnimation(string animation){
		if (animation == "hit") {
			yield return new WaitForSeconds (ReturnAnimationDuration (animation) / 2);
		}
		spineAnimationState.SetAnimation (0, animation, false).timeScale = 1 + (GetComponent<Character>().speedAdjustments[animation]/100);
		yield return new WaitForSeconds (ReturnAnimationDuration (animation));
		StartCoroutine (IdleAnimation ());
		yield return null;
	} 

	public float ReturnAnimationDuration(string animation){
		float duration = skeletonAnimation.Skeleton.Data.FindAnimation (animation).duration;
		return duration/ (1 +(GetComponent<Character>().speedAdjustments[animation]/100));
	}
}
