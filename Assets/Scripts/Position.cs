﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Position : MonoBehaviour {

	public int position;



	public void SetPosition(int value){
		position = value;
		gameObject.transform.localPosition = new Vector3 ((position*2.5f), -2f, -1f);

		if (gameObject == GameManager.enemy) {
			GameManager.player.GetComponent<Position> ().SetRotation ();
		} else {
			if (GameManager.enemy != null) {
				GameManager.enemy.GetComponent<Position> ().SetRotation ();
			}
		}
	}

	public IEnumerator MoveToPosition(int value, string animation){

		float moveDuration = GetComponent<Animations> ().ReturnAnimationDuration (animation);
		Vector3 currentPosition = gameObject.transform.position;
		Vector3 targetPosition = new Vector3 ((value) * 2.5f, -2f, -1f);
		float time = 0f;

		StartCoroutine(GetComponent<Animations>().PlayAnimation(animation));

		while (time < moveDuration) {
			transform.position = Vector3.Lerp (currentPosition, targetPosition, time / moveDuration);
			time += Time.deltaTime;
			yield return new WaitForEndOfFrame ();
		}
			
		yield return null;
	}



	public IEnumerator MovePosition(int value){
		if (value == 0) {
			yield break;
		}
		string animation;
		if (value > 0) {
			animation = "forward";
		} else {
			animation = "back";
		}
		int newPosition = DeterminePosition (value);
		if (newPosition == position) {
			yield break;
		}
		yield return StartCoroutine(MoveToPosition (newPosition, animation));
		SetPosition (newPosition);
		yield return null;
	}	

	public void SetRotation(){
		if (gameObject == GameManager.player) {
			if (position > GameManager.enemy.GetComponent<Position> ().position) {
				Quaternion finalRotation = Quaternion.Euler(0f, 180f, 0f);
				gameObject.transform.rotation = finalRotation;
				finalRotation = Quaternion.Euler(0f, 0f, 0f);
				GameManager.enemy.transform.rotation = finalRotation;
			} else {
				Quaternion finalRotation = Quaternion.Euler(0f, 0f, 0f);
				gameObject.transform.rotation = finalRotation;
				finalRotation = Quaternion.Euler(0f, 180f, 0f);
				GameManager.enemy.transform.rotation = finalRotation;
			}
		} else {
			if (position > GameManager.player.GetComponent<Position> ().position) {
				Quaternion finalRotation = Quaternion.Euler(0f, 180f, 0f);
				gameObject.transform.rotation = finalRotation;
				finalRotation = Quaternion.Euler(0f, 0f, 0f);
				GameManager.player.transform.rotation = finalRotation;
			} else {
				Quaternion finalRotation = Quaternion.Euler(0f, 0f, 0f);
				gameObject.transform.rotation = finalRotation;
				finalRotation = Quaternion.Euler(0f, 180f, 0f);
				GameManager.player.transform.rotation = finalRotation;
			}
		}
	}

	int DeterminePosition(int value){
		string direction;

		if (gameObject == GameManager.player) {
			if (gameObject.GetComponent<Position> ().position > GameManager.enemy.GetComponent<Position> ().position) {
				direction = "left";
			} else {
				direction = "right";
			}
		} else {
			if (gameObject.GetComponent<Position> ().position > GameManager.player.GetComponent<Position> ().position) {
				direction = "left";
			} else {
				direction = "right";
			}
		}
		if (direction == "left") {
			value -= (value * 2);
		}

		int newPosition = position + value;

		if (newPosition > 3) {
			newPosition = 3;
		} 
		if (newPosition < -3) {
			newPosition = -3;
		}
			
		if (gameObject == GameManager.player) {
			if (newPosition == GameManager.enemy.GetComponent<Position> ().position) {
				if (direction == "left") {
					newPosition += 1;
				} else {
					newPosition -= 1;
				}
			}
		} else {
			if (newPosition == GameManager.player.GetComponent<Position> ().position) {
				if (direction == "left") {
					newPosition += 1;
				} else {
					newPosition -= 1;
				}
			}
		}

		return newPosition;
	}
}
